Logiciel éducatif consistant à regrouper des images par catégories.

Adaptation du logiciel "Association d'images" inclus dans le lanceur d'applications [Clicmenu](http://pragmatice.net/clicmenu3/index.htm).

L'application peut être testée en ligne [ici](https://primtux.fr/applications/association-images/index.html)

La licence ne s'applique pas aux images à classer qui appartiennent à Didier Bigeard, auteur de l'application originale incluse dans clicmenu. Il n'a pas été possible de le joindre pour lui demander son autorisation de mise sous licence GNU-GPL.
